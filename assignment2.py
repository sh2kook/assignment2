from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    json = jsonify(books)
    return json

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
    	new_Name = request.form['name']
    	new_id = str(len(books) + 1)
    	books.append({'title': new_Name, 'id': new_id})
    	return redirect(url_for('showBook', books = books))

    else:
    	return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'POST':
		new_Name = request.form['name']
		books[book_id - 1]['title'] = new_Name
		return redirect(url_for('showBook', books = books))

	else:
		return render_template('editBook.html', book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	title = books[book_id-1]['title']

	if request.method == 'POST':
		delete = books[book_id-1]
		books.remove(delete)

		for i in range(len(books)):
			books[i]['id'] = i + 1

		return redirect(url_for('showBook', books = books))

	else:

		return render_template('deleteBook.html', book_id = book_id, title = title)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

